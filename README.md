#TETRIS GROUP PROJECT
THE GOAL: A version of tetris using ncurses and C++


##AUTHORS
- Matthew Struble - mstrubl2@uoregon.edu
- Samuel Lundquist - slundqui@uoregon.edu
- Qi Han - qhan@uoregon.edu

##Makefile Usage
- $ make build //compiles the c++ code to 'tetris'
- $ make run //runs the game ./tetris
- $ make clean //removes test object file

